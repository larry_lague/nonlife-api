<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Received extends Model
{
    public $connection = 'local';
    public $table = 'received';
    public $timestamps = false;
    public $primarykey = 'id';  

    protected $fillable = [
        'status',
        'datetrans',
        'productName',
        'warehouseid'
    ];

    public function warehouseMain(){
        return $this->belongsTo(received::class, 'keyid');
    }
}
