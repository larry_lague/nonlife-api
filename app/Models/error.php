<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class error extends Model
{
    protected $connection = 'local';
    protected $table = 'error';
    public $timestamps = false;
    public $primarykey = 'id';  

    protected $fillable = [
        'datetime',
        'errormessage',
        'warehouseId'
    ];
}
