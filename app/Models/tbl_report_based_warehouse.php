<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_report_based_warehouse extends Model
{
    public $connection = 'cloud';
    public $table = 'tbl_report_based_warehouse';
    public $timestamps = false;
    public $primarykey = 'keyid';  

    protected $fillable = [
        'insertdate',
        'quote_referenceid',
        'quote_referenceno',
        'quote_date',
        'variantid',
        'variantname',
        'policy',
        'productNo',
        'productName',
        'productOptionNo',
        'productOptionName',
        'assured_no',
        'assured_name',
        'assured_type',
        'invoice_no',
        'invoice_date',
        'pr_no',
        'pr_date',
        'or_no',
        'or_date',
        'policyno',
        'printedpolicyno',
        'lto_auth_no_ctpl',
        'lto_coc_no_ctpl',
        'pol_issued_date',
        'pol_effective_date',
        'pol_expiry_date',
        'basic_premium',
        'vat',
        'lgt',
        'fst',
        'misc',
        'compfee',
        'docstamp',
        'cac_login_id',
        'cac_coop_no',
        'cac_coop_name',
        'cac_coop_branch',
        'cac_collection_fee',
        'cac_collection_fee_waived',
        'agent_login_id',
        'agent_type',
        'agent_name',
        'agent_collection_fee',
        'agent_collection_fee_waived',
        'agent_coll_fee_wtax',
        'gam_login_id',
        'gam_collection_fee',
        'gam_collection_fee_waived',
        'clifsa_collection_fee',
        'clifsa_collection_fee_waived',
        'total_collection_fee',
        'netpremium'
        
    ];

    public function warehouseCopy(){
        return $this->hasOne(received::class, 'warehouseid','keyid' );
    }
}
