<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Models\Received;
use App\Models\tbl_report_based_warehouse;
use Illuminate\Support\Facades\DB;
use DateTime;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    
    {
        $schedule->call(function () {
           
            $result = tbl_report_based_warehouse::where('status', 1)
                ->limit(20)
                ->get();

            DB::beginTransaction();
            try{ 
                foreach($result as $value) {
                    received::create([
                        'datetrans' => new DateTime(),
                        'status' => 0,
                        'productName' =>$value->productName,
                        'warehouseid'=>$value->keyid
                    ]);
                    $update =tbl_report_based_warehouse::where('keyid', $value->keyid)
                        ->update(['status'=> 0]);
                }  
                DB::commit();
                return response()->json([
                    'msg'=>"succes",
                    'success'=>true
                ]);

            } 
            catch (\Exception $e){
                DB::rollback();
            }  
            
            })
            ->everyMinute();
    }
}
