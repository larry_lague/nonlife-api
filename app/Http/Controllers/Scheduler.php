<?php

namespace App\Http\Controllers;

use App\Models\tbl_report_based_warehouse;
use App\Models\Received;
use App\Models\error;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class Scheduler extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        // $result = tbl_report_based_warehouse::where('status', 1)
        //         ->limit(20)
        //         ->get();

        // DB::beginTransaction();
        // try{ 

        //     foreach($result as $value) {
        //         received::create([
        //             'datetrans' => new DateTime(),
        //             'status' => 0,
        //             'productName' =>$value->productName,
        //             'warehouseid'=>$value->keyid
        //         ]);
        //         $update =tbl_report_based_warehouse::where('keyid', $value->keyid)
        //             ->update(['status'=> 0]);
        //     }  
        //     DB::commit();
        //     return response()->json([
        //         'msg'=>"succes",
        //         'success'=>true
        //     ]);

        // } 
        // catch (\Exception $e){
        //   DB::rollback();
        // }  
         
   
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tbl_report_based_warehouse  $tbl_report_based_warehouse
     * @return \Illuminate\Http\Response
     */
    public function show(tbl_report_based_warehouse $tbl_report_based_warehouse, received $received )
    {
        $data = $tbl_report_based_warehouse
        ->with('warehouseCopy') 
        ->get();

        return view('failedjob',[
            'data' =>$data,
            'dataTrans' =>  $received->get()->count(),
            'cloudTotal' =>  $tbl_report_based_warehouse->get()->count(),
            'transpending' =>  count(collect($data)
                ->where('warehouseCopy', null)
            )
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tbl_report_based_warehouse  $tbl_report_based_warehouse
     * @return \Illuminate\Http\Response
     */
    public function edit(tbl_report_based_warehouse $tbl_report_based_warehouse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tbl_report_based_warehouse  $tbl_report_based_warehouse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tbl_report_based_warehouse $tbl_report_based_warehouse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tbl_report_based_warehouse  $tbl_report_based_warehouse
     * @return \Illuminate\Http\Response
     */
    public function destroy(tbl_report_based_warehouse $tbl_report_based_warehouse)
    {
        //
    }
}
