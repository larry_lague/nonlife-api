<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

</head>




<div class="container">

<h2>Data Transfer Informations</h2>
<h4>Total Data From Cloud: {{ $cloudTotal }}</h4>
<h4>Total Transfered: {{$dataTrans}}</h4>
<h4>Total Pending for transfer: {{ $transpending}}</h4>
<br>
<h4>This table show all pending for transfer (update every 5 minutes)</h4>

<table class="table table-bordered">
    <thead>
        <tr>
            <th> ID</th>
            <th> Variant ID</th>
            <th> Variant Name </th>
            <th> Product Name </th>
            <th> Assure Type</th>
            <th> Policy Number </th>
        </tr>
    </thead>
    <tbody>
         @foreach($data as $data)

         @if ( $data['warehouseCopy'] == null )
          <tr>
              <td> {{$data->keyid}} </td>
              <td> {{$data->variantid}} </td>
              <td> {{$data->variantname}} </td>
              <td> {{$data->productName}} </td>
              <td> {{$data->assured_type}} </td>
              <td> {{$data->policyno}} </td>
          </tr>
          @endif
         @endforeach
   </tbody>
</table>

</div>