
<?php 
    return[
        'default' => env('DB_CONNECTION', 'cloud'),
        
        'connections' => [   
            'cloud' => [
                'driver'    => 'mysql',
                'host'      => env('DB_HOST'    ,null),
                'port'      => env('DB_PORT'    ,null),
                'database'  => env('DB_DATABASE',null),
                'username'  => env('DB_USERNAME',null),
                'password'  => env('DB_PASSWORD',null),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false
            ],

            'local' => [
                'driver'    => 'mysql',
                'host'      => env('DB_HOST1'    ,null),
                'port'      => env('DB_PORT1'    ,null),
                'database'  => env('DB_DATABASE1',null),
                'username'  => env('DB_USERNAME1',null),
                'password'  => env('DB_PASSWORD1',null),
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
                'strict'    => false
            ]

        ]

    ];
?>
